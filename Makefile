#Make Files for P3
CXXFLAGS += --std=c++14

all: output

debug: CXXFLAGS += -g
debug: test


test: test.o library.o publication.o 
	$(CXX) $(CXXFLAGS) -o test test.o library.o publication.o 
	./test

output: main.o library.o publication.o 
	$(CXX) $(CXXFLAGS) -o main main.o library.o publication.o
	./main

main.o: main.cpp *.h
	$(CXX) $(CXXFLAGS) -c main.cpp

library.o: library.cpp *.h
	$(CXX) $(CXXFLAGS) -c library.cpp

publication.o: publication.cpp *.h
	$(CXX) $(CXXFLAGS) -c publication.cpp


clean: 
	-rm -f *.o .gch *~ main
