#include <string>
#include <stdexcept>
#include <vector>
#include "publication.h"

using namespace std;

class Library {
public:
    void add_publication(Publication pub);
    void check_in(int publication_index);
    void check_out(int publication_index, string patron_name, string patron_phone);
    string publications_to_string(int publication_index);
    int number_of_publications();
private:
    vector<Publication> publications;
};

