#include <iostream>
#include <string>
#include "library.h"
#include "publication.h"

using namespace std;

void show_menu(){
    cout << "===============================\n"
            "C1325 Library Management System\n"
            "===============================\n"
            "Publications\n"
            "------------\n"
            "(1) Add publication\n"
            "(2) List all publications\n"
            "(3) Check out publication\n"
            "(4) Check in publication\n"
            "Utility\n"
            "-------\n"
            "(9) Help\n"
            "(0) Exit\n"
            "-------\n";

}

void list_publications(){

}

void execute_cmd(int cmd){

}


int main() {
    Library UTALib;

    int menu_op;
    menu_op = -1;

    while(true){
      show_menu();
      cin >> menu_op;
    switch (menu_op) {
        case 1:
        {
            //create new publication obj and add to vector of publications
            string new_title;
            string new_author;
            string new_copyright;
            int new_media;
            int new_genre;
            int new_age;
            string new_ISBN;

            cout << "Enter Information Below as Follows: (hit enter key after each item)\n"
                 << "\tPublication Name\n"
                 << "\tPublication Author (Firstname Lastname)\n"
                 << "\tPublication Copyright\n"
                 << "\tPublication Target Age:\n"
                 << "\t\t1)Children\n\t2)Teen\n\t3)Adult\n\t4)Restricted\n"
                 << "\tPublication Genre:\n"
                 << "\t\t1)Fiction\n\t2)Non-Fiction\n\t3)self-help\n\t4)Performance\n"
                 << "\tPublication Media:\n"
                 << "\t\t1)Book\n\t2)Periodicals\n\t3)Newspapers\n\t4)Audio\n\t5)Video\n"
                 << "\tPublication ISBN\n";
            cin.ignore();
            getline(cin, new_title);
            getline(cin, new_author);
            getline(cin, new_copyright);
            cin >> new_age;
            cin >> new_genre;
            cin >> new_media;
            cin.ignore();
            getline(cin, new_ISBN);
            Media media = static_cast<Media>(new_media);
            Genre genre = static_cast<Genre>(new_genre);
            Age age = static_cast<Age>(new_age);

            Publication new_publication(new_title, new_author, new_copyright, age, genre, media, new_ISBN);
            UTALib.add_publication(new_publication);
        }
            break;
        case 2:
        
            //List all publications from Vector<publications>
            for(int i=0; i<UTALib.number_of_publications(); ++i){
                cout << "Locator: " <<(i+1) << "\n" <<  UTALib.publications_to_string(i) << endl;
            }
            break;
        case 3:
        {
            //Find Publication in vector<publications> call checkout method with info
            int publication_index;
            string new_patron_name;
            string new_patron_phone;

            
            cout << "Enter the Index Number to CheckOUT: ";
            cin >> publication_index;
            cout << "Enter Patron Name: ";
            cin.ignore();
            getline(cin, new_patron_name);
            cout << "Enter Patron Phone Number: ";
            cin >> new_patron_phone;

            
            UTALib.check_out((publication_index-1), new_patron_name, new_patron_phone);
        }
            break;
        case 4:
        {
            //Find Publication in Vector<publications> call checkin method
            int publication_index;
            
            cout << "Enter the Index Number to CheckIN: ";
            cin >> publication_index;
            
            UTALib.check_in((publication_index-1));
        }
            break;
        case 9:
            //Print Help function or directly menu.
            cout << "================================\n"
                    "Readme/Help Guide for C1325 LMS!\n"
                    "================================\n"
                    "Use the CLS commands to manage library, information the LMS stores includes but not limited to: ISBM, Patron Name & Phone Number, Index, and more.\n"
                    "Publications(books, magazines, etc) are stored using an Index/Locator the LMS Index starts at 1 and incements 1 for every addtional publication e.g. 1-100 for 100 Objects.\n"
                    "Locators are printed 1 line above the publications information.\n";

            break;
        case 0:
            cout << "===============================\n"
                    "Thanks for using the C1325 LMS!\n"
                    "\t\t\tGoodBye!\n"
                    "===============================\n";
                    return 0;
            break;
        case 42:
            //Print Help function or directly menu.
            cout << "================================\n"
                    "Readme/Help Guide for C1325 LMS!\n"
                    "================================\n"
                    "Use the CLS commands to manage library, information the LMS stores includes but not limited to: ISBM, Patron Name & Phone Number, Index, and more.\n"
                    "Publications(books, magazines, etc) are stored using an Index/Locator the LMS Index starts at 1 and incements 1 for every addtional publication e.g. 1-100 for 100 Objects.\n"
                    "Locators are printed 1 line above the publications information.\n";

            break;
        default:
            cout << "An Error has occured, make sure you enter 1-4, 9, or 0";
            break;
     }
}



    return 0;
}
