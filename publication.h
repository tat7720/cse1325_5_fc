#pragma once
#include <string>
#include <stdexcept>
#include "enums.h"

using namespace std;

class Publication {
public:
    Publication(string new_title, string new_author, string new_copyright, Age new_target_age, Genre new_genre, Media new_media, string new_isbn);
    void check_out(string patron_name, string patron_phone);
    void check_in();
    bool is_checked_out();
    string to_string();
    //friend ostream& operator<<(ostream& os, const Publication publication);
private:
    string title;
    string author;
    string copyright;
    Genre genre;
    Media media;
    Age target_age;
    string isbn;
    bool checked_out = false;
    string patron_name;
    string patron_phone;
};
