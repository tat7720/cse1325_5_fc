#include <string>
#include <iostream>
#include <sstream>
#include "publication.h"

using namespace std;


Publication::Publication(string new_title, string new_author, string new_copyright, Age new_target_age, Genre new_genre, Media new_media, string new_isbn) :
title{new_title}, author{new_author}, copyright{new_copyright}, target_age{new_target_age}, genre{new_genre}, media{new_media}, isbn{new_isbn} { };

void Publication::check_out(string ppatron_name, string ppatron_phone){
    checked_out = true;
    patron_name += ppatron_name;
    patron_phone += ppatron_phone;
};

void Publication::check_in(){
    checked_out = false;
    patron_name.clear();
    patron_phone.clear();
};

bool Publication::is_checked_out(){
    return checked_out;
};


string Publication::to_string(){
    stringstream ss;
    ss << "\"" << title << "\" by " << author
       << ", " << copyright << " (" << age_to_string[(target_age-1)] << " " << genre_to_string[(genre-1)] << " " << media_to_string[(media-1)] << ") "
    << "ISBN: " << isbn;
    if(checked_out == true){
        ss << " Checked out to " << patron_name << " (" << patron_phone << ") ";
    }
    return ss.str();
};


//“The Firm” by John Grisham, 1991 (adult fiction book) ISBN: 0440245923 Checked out to Professor Rice (817-272-3785)

