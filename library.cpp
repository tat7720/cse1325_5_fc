#include <string>
#include <iostream>
#include "library.h"

using namespace std;

void Library::add_publication(Publication pub){
  publications.push_back(pub);
};

void Library::check_in(int publication_index){
    Library::publications[publication_index].check_in();
};

void Library::check_out(int publication_index, string patron_name, string patron_phone){
  Library::publications[publication_index].check_out(patron_name,patron_phone);
};

string Library::publications_to_string(int publication_index){
    return publications[publication_index].to_string();
};

int Library::number_of_publications(){
    return (int)publications.size();
};
