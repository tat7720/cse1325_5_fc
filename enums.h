#ifndef enums_h
#define enums_h
#include <iostream>
#include <string>
#include <vector>

using namespace std;

enum Media { book, periodical, newspaper, audio, video };
const vector<string> media_to_string = {"book", "periodical", "newspaper", "audio", "video"};

enum Genre { fiction, non_fiction, self_help, performance };
const vector<string> genre_to_string = {"fiction", "non-fiction", "self-help", "performance"};

enum Age { children, teen, adult, restricted };
const vector<string> age_to_string = {"children", "teen", "adult", "restricted"};


#endif
